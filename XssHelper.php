<?php

/**
 * Base class for text xss filtering
 *
 * @since  1.1
 * @author Tomasz Załucki <z4lucki@gmail.com>
 */

namespace common\helpers;

class XssHelper
{

    /**
     * Catch all get and Xss filter
     *  
     * @param any $value
     * @param integer $intval
     *
     * @return array
     */
    public static function get($value, integer $intval) : string
    {
        if (is_array($value)) {
            //array_walk_recursive($value, array(__CLASS__, 'filterGet'));
            $value = self::recursiveGet($value);
        } else {
            $value = self::strong($value, $intval, 'get');
        }

        return $value;
    }

    /**
     * Catch all post and Xss filter
     *  
     * @param array $value
     * @param integer $intval
     *
     * @return array
     */
    public static function post($value, $intval)
    {
        if (is_array($value)) {
            //array_walk_recursive($value, array(__CLASS__, 'filterPost'));
            $value = self::recursivePost($value);
        } else {
            $value = self::strong($value, $intval, 'post');
        }
        return $value;
    }

    /**
     * Bridget function for @see XssHelper::get         Multi Get Xss Haleper
     *  
     * @date 10.04.2019
     * The function is not used
     * 
     * @param array $value
     * @param integer $intval
     *
     * @return array
     */
    public static function filterGet($value, $intval)
    {
        return self::strong($value, $intval, 'get');
    }

    /**
     * Bridget function for @see XssHelper::post         Multi :post Xss Haleper
     *  
     * @date 10.04.2019
     * The function is not used
     * 
     * @param array $value
     * @param integer $intval
     *
     * @return array
     */
    public static function filterPost(&$value, $intval)
    {
        return self::strong($value, $intval, 'post');
    }

    /**
     * Array recursive function with map function and callback to parent function.
     * 
     * GET
     * 
     * @param type $input
     * @return type
     */
    public static function recursiveGet($array)
    {
        if (is_array($array)) {
            return array_map(array(__CLASS__, 'recursiveGet'), $array);
        } else if (is_scalar($array)) {
            return self::strong($array, false, 'get');
        } else {
            return $array;
        }
    }

    /**
     * Array recursive function with map function and callback to parent function.
     * 
     * POST
     * 
     * @param type $input
     * @return type
     */
    public static function recursivePost($array)
    {
        if (is_array($array)) {
            return array_map(array(__CLASS__, 'recursivePost'), $array);
        } else if (is_scalar($array)) {
            return self::strong($array, false, 'post');
        } else {
            return $array;
        }
    }

    /**
     * Strong function delete all xss tags and html tags with their content.
     * 
     * @param string $value
     * @param int $intval
     * @param void $sql

     * @param string $name
     *
     * @return string
     */
    public static function strong($value, $intval = false, $sql = false, $name = false)
    {


        if ($sql === "get") {

            /**
             * Strip tags. 
             * Remove all html tag and context.
             */
            $value = trim(htmlspecialchars(self::strip_tags_content($value), ENT_COMPAT | ENT_HTML401, 'UTF-8', false));

            /**
             *  With word book.
             */
            $value = str_replace(self::sqlGet(), "", $value);
            /*
             * Additional regex replace.
             * @return only letters, numbers and -#,./
             */
            $value = preg_replace("/[^A-Za-z0-9-#,.@_ąćęłńóśźżĄĆĘŁŃÓŚŹŻ\/ ]/", '', $value);
        }
        if ($sql === "post") {

            /**
             * Strip tags without html special chars funtion 
             * Remove all html tag and context.
             */
            $value = trim(self::strip_tags_content($value));

            /**
             *  With word book.
             */
            $value = str_replace(self::sqlPost(), "", $value);
        }


        if ($intval == "intval") {
            $value = intval($value);
        }

        return $value;
    }

    public static function filterColumn(array $column, array $filter)
    {

        /*
         * filterColumn
         * Delete model's column from @array $filter
         */
        return array_diff($column, $filter);
    }

    /**
     * Strip tags around word with html tag
     *
     * @return array
     */
    private static function strip_tags_content($text, $tags = '', $invert = FALSE)
    {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == FALSE) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    /**
     * Dictionary sql injection
     *
     * @return array
     */
    private static function sqlGet()
    {
        $words = [
            '(SELECT',
            '( SELECT',
            '(select',
            '( select',
            '^',
            '$',
            ',',
            ')',
            '(',
            '1=1',
            '1=0',
            '0=0',
            '1/0',
            '1/1',
            '0/0',
            '0=1',
            "'1'='1'",
            "'1'='1",
            "'1'=1",
            '=',
            '&&',
            '||',
            '|',
            "'",
            '"',
            'NULL',
            'UNION ALL select',
            'UNION ALL',
            'UNION',
            '<script>',
            '<Script>',
            '<SCRIPT>',
            'alert',
            '</>',
            ') or',
            ')or(',
            ') or (',
            ' or ',
            '@@version',
            'substring',
        ];
        return $words;
    }

    /**
     * Dictionary sql injection
     *
     * @return array
     */
    private static function sqlPost()
    {
        $words = [
            '(SELECT',
            '( SELECT',
            '(select',
            '( select',
            '^',
            '1=1',
            '1=0',
            '0=0',
            '1/0',
            '1/1',
            '0/0',
            '0=1',
            "'1'='1'",
            "'1'='1",
            "'1'=1",
            '&&',
            '||',
            '|',
            'NULL',
            'UNION ALL select',
            'UNION ALL',
            ' UNION ',
            '<script>',
            '<Script>',
            '<SCRIPT>',
            'alert',
            '</>',
            ') or',
            ')or(',
            ') or (',
            '@@version',
            'substring',
            ' AS ',
            ' as ',
        ];
        return $words;
    }

}
