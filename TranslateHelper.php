<?php

namespace common\helpers;

use Yii;
use common\modules\translation\models\Translation;
use common\modules\translation\models\TranslationLanguage;
use common\modules\translation\models\Translation_i18n;

/**
 * TranslateHelper Class
 *
 * author Tomasz Załucki <z4lucki@gmail.com>
 * @since 2019
 */

class TranslateHelper
{

    public $edit;

    /**
     *
     * Method for webiste translates
     * Management in admin panel
     *
     * @param type $sCode
     * @param type $translation
     * @return type
     */
    public static function t(string $sCode = null, strng $translation = null) : string
    {
        $translate = Yii::$app->liteCache->get("translation", $sCode);
        
        if(!$translate){
            /**
             * Read model
             */
            $oTranslation = Translation::find()->where(["=", "code", $sCode])->one();

    //        if($sCode=='Opinion:'){
            //            var_dump($translation).exit();
            //        }
            if (is_null($oTranslation)) {

                /**
                 * Add new translate to database
                 */
                $oTranslation = Translation::create($sCode, $translation, true);

                /**
                 * If translate exist and we change polsih translation (second argument)
                 */
                // } elseif ($translation && $oTranslation->getTranslation(self::$mainLanguage) != $translation) {
            } elseif ($translation && $oTranslation->site != $translation) {

                /**
                 * Get main language, which you will setting in admin panel on translate module
                 */
                $mainLanguage = TranslationLanguage::find()->where(['main' => 1])->asArray()->one();

                /**
                 * Replace pl translate
                 */
                $i18n = Translation_i18n::find()->where(
                    [
                        'translation_id' => $oTranslation->id,
                        'language' => isset($mainLanguage['language']) ? $mainLanguage['language'] : 'pl-PL',
                    ])->one();

                if ($i18n) {
                    $i18n->translation = htmlspecialchars($translation);
                    $i18n->save();
                }
                /**
                 * Replace site translate
                 */
                $oTranslation->site = htmlspecialchars($translation);
                $oTranslation->save();
            }
            $translate = $oTranslation->getTranslation();
            Yii::$app->liteCache->set("translation", $sCode, $translate);
        }

        return $translate;
    }

}
