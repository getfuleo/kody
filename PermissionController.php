<?php

namespace console\controllers;

use common\modules\modules\models\ModuleSearch;
use common\modules\permissions\models\GroupPermission;
use common\modules\permissions\models\Permission;
use common\modules\users\models\User;
use common\modules\users\models\Group;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Init permission , write to console: php yii permission/init
 */
class PermissionController extends Controller
{

    public $time_start = 0;
    public $time_end = 0;
    public $type = ["view", "update", "create", "delete"];

    public function beforeAction($action)
    {

        $this->time_start = microtime(true);

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {

        $this->time_end = microtime(true);
        $time = round($this->time_end - $this->time_start, 2);

        Console::stdout(Console::ansiFormat("Czas wykonania: {$time}s.\n", [Console::FG_YELLOW]));
        return parent::afterAction($action, $result);
    }

    /*
     * Generate permission set for module
     */

    public function actionGenerate($module = false)
    {
        Console::stdout(Console::ansiFormat("[START]\n", [Console::FG_YELLOW]));

        /**
         * Add permision to main panel
         */
        $this->savePermission(null, null, 'panel');

        /**
         * Find All modules
         */
        $modules = ModuleSearch::find()->orderBy(['display_name' => SORT_ASC])->all();

        //$modules = ModuleSearch::find()->where(['name' => 'Articles'])->all();

        foreach ($modules as $modul) {
            // 1. SAVE MODULE
            /**
             * Module save.
             * We checked exist tableName method and save to permission database.
             */
            if (method_exists($modul, 'tableName')) {
                $modulName = basename($modul->admin);
                Console::stdout(Console::ansiFormat($modulName . "\n", [Console::FG_BLUE]));
                // 0. GENERATE main module e.g. Articles
                $this->savePermission(null, $modulName, 'module');
                foreach ($this->type as $value) {
                    $this->savePermission(null, $modulName, $value);
                }
                /**
                 * Get all models from module
                 */
                if ($modul->model) {


                    foreach ($this->getModels($modul->model) as $model) {

                        // 2. SAVE MODEL

                        Console::stdout(Console::ansiFormat($model['model'] . "\n", [Console::FG_RED]));

                        $this->savePermission(null, $model['model'], 'module');


                        foreach ($this->type as $value) {
                            $this->savePermission(null, $model['model'], $value);
                        }
                        // 3. Save field from model
                        foreach ($model['attributes'] as $key => $field) {

                            // Console::stdout(Console::ansiFormat($key . "\n", [Console::FG_YELLOW]));
                            foreach ($this->type as $value) {
                                $this->savePermission($key, $model['model'], $value);
                            }
                        }
                    }
                } // if $model->model exist
            }
        }
    }
    public function actionTrans($group)
    {
 
        $group =  Group::findOne($group);
        if (!$group) {
            Console::stdout(Console::ansiFormat('Podana grupa nie istnieje!' . "\n", [Console::FG_RED]));
            exit;
        }



        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $value = '';
        $permissions = Permission::find()->select('id')->asArray()->all();
       
        foreach ($permissions as $perm) {
            $value .= "({$group->id},{$perm['id']}),";
        }
       

        $sql = "INSERT INTO bwcms_xmod_groups_permissions (group_id,permission_id) VALUES ".rtrim($value, ",").";";
        try {
            $connection->createCommand($sql)->execute();

            //.... other SQL executions
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    public function actionGroup($group)
    {

        $group = UserGroup::findOne($group);
        if (!$group) {
            Console::stdout(Console::ansiFormat('Podana grupa nie istnieje!' . "\n", [Console::FG_RED]));
            exit;
        }
        Console::stdout(Console::ansiFormat('Generuje wszystkie uprawnienia dla grupy: ' . $group->id . "\n", [Console::FG_BLUE]));


        $permissions = Permission::find()->all();
        $i = 0;
        foreach ($permissions as $perm) {

            $oModel = GroupPermission::find()->select('id')->where([
                'group_id' => $group->id,
                'permission_id' => $group->id,
            ])->asArray()->one();


            if (is_null($oModel)) {
                $model = new GroupPermission();
                $model->group_id = $group->id;
                $model->permission_id = $perm->id;
                if ($model->save()) {
                    $i++;
                } else {
                    echo '<pre>';
                    print_r($model->getErrors());
                    echo '</pre>';
                }
            }
        }
        Console::stdout(Console::ansiFormat('Wygenerowano ' . $i . ' uprawnien dla grupy: ' . $group->id . "\n", [Console::FG_GREEN]));
    }
    public function savePermission($name = null, $model = null, $type = null)
    {

        $oModel = Permission::find()->select('id')->where([
            'name' => $name,
            'model' => $model,
            'type' => $type,
        ])->asArray()->one();


        if (is_null($oModel)) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->model = $model;
            $permission->type = $type;
            $permission->save();
        }
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function getModels($modelName)
    {
        /**
         * Get namespace
         */
        $namespace = $this->getNamespace($modelName, true, true);

        /**
         * Search all Class from namespace directory
         */
        $files = FileHelper::findFiles(Yii::getAlias('@common') . '\\' . $namespace, ['only' => ['*.php'], 'caseSensitive' => FALSE]);

        /*
         * Get all methods from FileHelper loop.
         */
        $methods = [];
        foreach ($files as $index => $file) {
            $model = basename($file, ".php");
            $methods[$index]['model'] = $model;
            $methods[$index]['attributes'] = $this->getAttributes($namespace, $model);
        }

        return $methods;
        //        return $this->render('/modules/tables', [
        //                    'modules'   => $methods,
        //                    'namespace' => $namespace,
        //                    'group_id'  => $group,
        //        ]);
    }

    private function getNamespace(string $method, bool $shift = false, bool $pop = false): string
    {
        // $module    = Module::find()->where(['id' => $method])->one();
        $namespace = explode('\\', $method);
        /**
         * Remove first array key @common
         */
        if ($shift) {
            array_shift($namespace);
        }
        /**
         * Remove last array key with method name
         */
        if ($pop) {
            array_pop($namespace);
        }


        return implode('\\', $namespace);
    }

    public function getAttributes($namespace, $model)
    {
        $namespace = '\common\\' . $namespace . '\\' . $model;
        $modelNew = new $namespace;


        $attributes = [];
        if (method_exists($modelNew, 'tableName')) {
            $attributes = $modelNew->getAttributes();

            foreach ($attributes as $key => $value) {
                $attributes[$key] = $modelNew->getAttributeLabel($key);
            }
        }


        return $attributes;
    }
}
